. ~/.shell/aliases
. ~/.shell/completions
. ~/.shell/functions
. ~/.shell/variables
. ~/.shell/host_specific
. ~/.shell/prompt

# Run on new shell
#if [ `which fortune` ]; then
#    echo ""
#    fortune
#    echo ""
#fi
